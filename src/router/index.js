import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Books from '../views/Books.vue'
import BooksFavorite from '../components/BooksFavorite.vue'

Vue.use(VueRouter)

const routes = [
	{
		path: '/',
		name: 'Home',
		component: Home,
	},
	{
		path: '/books',
		name: 'Books',
		component: Books,
	},
	{
		path: '/books-favorite',
		name: 'Books Favorite',
		component: BooksFavorite,
	},
]

const router = new VueRouter({
	mode: 'history',
	routes,
})

export default router
