import Dexie from 'dexie'

export const db = new Dexie('booksDatabase')
db.version(1).stores({
	booksItem: '++id, booksId, activeClass, authors, averageRating, buyLink, description, image, listPrice, pageCount, printType, publishedDate, publisher, retailPrice, saleAbility, title',
})