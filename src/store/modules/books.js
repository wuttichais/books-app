import * as gettersType from '../getters_types'
import * as mutationType from '../mutations_type'
import * as actionsType from '../actions_types.js'
import { db } from '@/db/db.js'

const state = {
	searchText: '',
	booksItem: {},
	booksFavorite: [],
	errorSnackbar: false,
	errorMessage: ''
}

const mutations = {
	[mutationType.SET_SEARCH_TEXT]: (state, payload) => {
		state.searchText = payload
	},
	[mutationType.SET_BOOKS_ITEM]: (state, payload) => {
		state.booksItem = payload
	},
	[mutationType.SET_BOOKS_FAVORITE]: (state, payload) => {
		state.booksFavorite = payload
	},
	[mutationType.SET_ERROR_SNACKBAR]: (state, payload) => {
		state.errorSnackbar = payload
	},
	[mutationType.SET_ERROR_MESSAGE]: (state, payload) => {
		state.errorMessage = payload
	},
}

const getters = {
	[gettersType.GET_SEARCH_TEXT]: state => {
		return state.searchText
	},
	[gettersType.GET_BOOKS_ITEM]: state => {
		return state.booksItem
	},
	[gettersType.GET_BOOKS_FAVORITE]: state => {
		return state.booksFavorite
	},
	[gettersType.GET_ERROR_SNACKBAR]: state => {
		return state.errorSnackbar
	},
	[gettersType.GET_ERROR_MESSAGE]: state => {
		return state.errorMessage
	},
}

const actions = {
	[actionsType.FETCH_BOOKS_FAVORITE]: async ({ commit }) => {
		try {
			const result = await db.booksItem.toArray()
			commit(mutationType.SET_BOOKS_FAVORITE, result)
		} catch (error) {
			commit(mutationType.SET_ERROR_SNACKBAR, true)
			commit(mutationType.SET_ERROR_MESSAGE, error)
		}
	},
	[actionsType.ADD_BOOKS_FAVORITE]: async ({ commit }, item) => {
		try {
			await db.booksItem.add(item)
		} catch (error) {
			commit(mutationType.SET_ERROR_SNACKBAR, true)
			commit(mutationType.SET_ERROR_MESSAGE, error)
		}
	},
	[actionsType.DELETE_BOOKS_FAVORITE]: async ({ commit }, { booksId }) => {
		try {
			await db.booksItem.where('booksId').equals(booksId).delete()
		} catch (error) {
			commit(mutationType.SET_ERROR_SNACKBAR, true)
			commit(mutationType.SET_ERROR_MESSAGE, error)
		}
	},
}

export default {
	state,
	getters,
	mutations,
	actions
}